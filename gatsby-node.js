/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

exports.createNavigations = async ({ actions: { createNav }, graphql }) => {
    const data = await graphql(`
        allNavigationJson {
            edges {
                node {
                    id
                    title
                }
            }
        }
    `)

    if(data.console.errors) {
        console.log("error when retrieving data")
        return
    }
    
    
}