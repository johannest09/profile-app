import React from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import { Button } from '@material-ui/core'


import SectionHeader from '../components/sectionHeader'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles(theme => ({
    root: {
        position: "relative",
        maxWidth: "800px",
        margin: "0px auto",
        textAlign: 'center',
        padding: "100px 0 0 0",
        minHeight: '100vh',
        [theme.breakpoints.up('md')]: {
            padding: "150px 100px",
            minHeight: 0
        }
    },
    footer: {
        padding: `${theme.spacing(2)}px 0`,
        textAlign: 'center',
        fontFamily: 'Share Tech Mono',
        opacity: '0.7'
    }
}))

const StyledButton = withStyles(theme => ({
    root: {
        marginTop: theme.spacing(4),
        textTransform: 'none',
        fontWeight: '300',
        border: `1px solid ${theme.palette.secondary.main}`,
        fontFamily: 'Share Tech Mono'
    }
}))(Button)

const Contact = () => {

    const classes = useStyles()

    return (
        <>
            <section id="contact" className={classes.root}>

                <SectionHeader title="Let's talk" variant="h3" index={3} line={false} centered={true} />

                <Typography paragraph>
                    If you wan’t to know more about me and perhaps have a discussion about problems / challenges where my skills could contribute, feel free to reach me on email <strong>johannesfreyr@gmail.com</strong> or click the button.
                </Typography>

            <StyledButton href="mailto:johannesfreyr@gmail.com" color="secondary" size="large" variant="contained">Say hello!</StyledButton>


            </section>

            <footer className={classes.footer}>
                <div>Designed & built by Jóhannes Freyr Þorleifsson</div>
            </footer>
        </>
    )
}

export default Contact