import React from 'react'
import { Helmet } from "react-helmet"

const Head = () => {
    return (
        <Helmet>
            <link href="https://fonts.googleapis.com/css2?family=Saira:wght@300;400;500;600&display=swap" rel="stylesheet" />
            <link href="https://fonts.googleapis.com/css2?family=Share+Tech+Mono&display=swap" rel="stylesheet" />
        </Helmet>
    )
}

export default Head