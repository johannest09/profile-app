import React from 'react'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import EmailIcon from '@material-ui/icons/Email'

const Home = () => {

    const useStyles = makeStyles(theme => ({
        hero: {
            maxWidth: '1000px',
            display: 'flex',
            WebkitBoxPack: 'center',
            justifyContent: 'center',
            WebkitBoxAlign: 'center',
            flexDirection: 'column',
            alignItems: 'flex-start',
            minHeight: '100vh',
            margin: '0px auto',
        },
        subtitle: {
            maxWidth: '700px',

        },
        proffessions: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(10),
            '& > h3': {
                marginBottom: theme.spacing(3)
            },
            '& > ul': {
                listStyle: 'none',
                margin: '0',
                padding: '0',
                '& li': {
                    padding: `${theme.spacing(1/2)}px 0 0 ${theme.spacing(2)}px`,
                    position: 'relative',
                    fontWeight: '300',
                    '&:before': {
                        content: '\'▹\'',
                        position: 'absolute',
                        left: '0px',
                        color: theme.palette.secondary.main,
                    }
                }
            },
        }
    }))

    const StyledHeading = withStyles({
        root: {
            padding: '1.5rem 0'
        }
        
    })(Typography)

    const StyledButton = withStyles(theme => ({
        root: {
            textTransform: 'none',
            fontWeight: '300',
            border: `1px solid ${theme.palette.secondary.main}`
        }
    }))(Button)

    const classes = useStyles()

    return (
        <section className={classes.hero} id="home">
            <StyledHeading 
                variant="h2" 
                component="h2" 
                color="secondary">
                Web developer <br />Graphic designer
            </StyledHeading>
            <Typography className={classes.subtitle}>
                I’m a software developer / graphic designer based in Reykjavik, Iceland, specializing in building and designing amazing websites and applications.
            </Typography>
            <div className={classes.proffessions}>
                <Typography variant="h3" component="h3">I do</Typography>
                <ul>
                    <li>Web design and UX</li>
                    <li>Wireframing and prototyping</li>
                    <li>Front-end development</li>
                    <li>General software development</li>
                </ul>
            </div>

            <StyledButton 
                variant="outlined" 
                color="secondary" 
                size="large"
                startIcon={ <EmailIcon /> }
                >Get In Touch</StyledButton>

        </section>
    )
}

export default Home