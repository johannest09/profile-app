import React from "react"
import { StaticQuery, graphql } from "gatsby"
import { makeStyles } from '@material-ui/core/styles'

import Img from "gatsby-image"


const HoverImage = (props) => (
  <StaticQuery
    query={graphql`
        query {
          images: allFile {
            edges {
              node {
                relativePath
                name
                childImageSharp {
                  sizes(maxWidth: 600) {
                    ...GatsbyImageSharpSizes
                  }
                }
              }
            }
          }
        }
      `}

    render={(data) => {

      const image = data.images.edges.find(n => {
        return n.node.relativePath.includes(props.filename);
      });
      if (!image) { return null; }

      const imageSizes = image.node.childImageSharp.sizes;


      const classes = makeStyles(theme => ({
        root: {
          position: 'absolute',
          left: 0,
          right: 0,
          opacity: props.active ? 1 : 0,
          transition: 'all 250ms ease-in-out',
          borderRadius: '3px',
          overflow: 'hidden',
        }
      }))()

      return (
        <div className={classes.root}>
          <Img
            alt={props.alt}
            sizes={imageSizes}
          />
        </div>

      )
    }}
  />
)

export default HoverImage