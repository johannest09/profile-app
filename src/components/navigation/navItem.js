import React from 'react'
import { Link } from 'react-scroll'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

import CV from '../../images/johannes_Freyr_Thorleifsson_Resume_EN.pdf'

const stagger = 100;

const useStyles = makeStyles((theme) => ({
    navLink: {
        display: 'inline-block',
        textDecorationSkipInk: 'auto',
        position: 'relative',
        cursor: 'pointer',
        textDecoration: 'none',
        transition: 'all 150ms ease-in',
        '& > .active': {
            fontWeight: 'bold',
        },
        '&:hover': {
            color: theme.palette.secondary.main,
        },
    },
    navLinkNr: {
        color: theme.palette.secondary.main,
        marginRight: theme.spacing(1)
    },
    test: {
        transform: 'translateY(100%)',
        transitionDuration: '0.5s',
        transitionTimingFunction: 'ease-in-out',
        transitionProperty: 'transform',
        transitionDelay: `${stagger * 1}ms`
    }
}))


const NavItem = ({ item, index }) => {

    const classes = useStyles()

    const StyledButton = withStyles(theme => ({
        root: {
            textTransform: 'none',
            fontWeight: '300',
            border: `1px solid ${theme.palette.secondary.main}`
        }
    }))(Button)

    if(item.id === 'cv') {
        return <StyledButton variant="outlined" color="secondary" href={ CV } target="_blank">Resume</StyledButton>
    } else {
        return (
            <Link activeClass="active" to={item.id} spy={true} smooth={true} offset={-70} duration={500} className={classes.navLink}>
                <span className={classes.navLinkNr}>{'0' + (index + 1) + '. '}</span>{item.title}
            </Link>
        )
    }

    
}

export default NavItem