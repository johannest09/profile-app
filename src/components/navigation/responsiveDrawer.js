import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { Link, Events } from "react-scroll";
import Button from '@material-ui/core/Button'

import { useStaticQuery, graphql } from "gatsby"
import CV from '../../images/johannes_Freyr_Thorleifsson_Resume_EN.pdf'

import useScrollPosition from '../navigation/useScrollPosition'


import HamburgerMenu from './hamburgerMenu'

const drawerWidth = 280;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    mobileNav: {
        left: '0',
        top: '0',
        width: '100%',
        display: 'flex',
        zIndex: '1000',
        position: 'fixed',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: theme.palette.primary.main,
        padding: `0 ${theme.spacing(4)}px`,
        visibility: 'visible',
        boxShadow: 'rgba(2, 12, 27, 0.7) 0px 10px 30px -10px',
        transition: 'all 0.25s cubic-bezier(0.645, 0.045, 0.355, 1) 0s',
    },
    scrolling: {
        boxShadow: 'rgba(2, 12, 27, 0.7) 0px 10px 30px -10px',
		visibility: 'hidden',
		transition: 'all 0.25s cubic-bezier(0.645, 0.045, 0.355, 1) 0s',
		transform: 'translate(0, -100%)'
    },
    top: {
		boxShadow: 'none',
	},
    navLink: {
        '> .active': {
            fontWeight: 'bold',
        }
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
        backgroundColor: theme.palette.secondary.main
    },
    content: {
        flexGrow: 1,
        padding: `0 ${theme.spacing(3)}px`,
    },
    hiddenMobile: {
        width: '0%'
    }
    
}))

const StyledButton = withStyles(theme => ({
    root: {
        fontWeight: '500',
        marginTop: theme.spacing(4)
    }
}))(Button)

let navItems = []

const ResponsiveDrawer = (props) => {
    const { window, children } = props
    const classes = useStyles()

    const [mobileOpen, setMobileOpen] = useState(false)
    const [hideOnScroll, setHideOnScroll] = useState(false)
    const [isTop, setIsTop] = useState(true)

    const { allNavigationJson } = useStaticQuery(
        graphql`
            query {
                allNavigationJson {
                    edges {
                        node {
                            id
                            title
                        }
                    }
                }
            }
        `
      )

    if(navItems.length === 0) {
        for(let i = 1; i < allNavigationJson.edges.length; i++) {
            navItems.push(allNavigationJson.edges[i])
        }
    }

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen)
    }

    useScrollPosition(({ prevPos, currPos }) => {
		const isShow = currPos.y < prevPos.y
		if (isShow !== hideOnScroll) setHideOnScroll(isShow)

		setIsTop(currPos.y > -50)

    }, [hideOnScroll], false, false, 300)
    
    useEffect(() => {
        Events.scrollEvent.register('end', () => setMobileOpen(false))
        return () => {
            Events.scrollEvent.remove('end')
        }
    },[])

    const menuList = (
        <aside>
            <div className={classes.toolbar} />
            <HamburgerMenu color="primary" onClick={handleDrawerToggle} aria-label="open-drawer" open={ mobileOpen } sidebar />
            <Divider />
            <List>
                {navItems.map((navItem, index) => (
                    <ListItem button key={navItem.node.id} >
                        {
                            navItem.node.id === 'cv' ? <StyledButton variant="contained" color="primary" size="large" href={ CV } target="_blank">Resume</StyledButton> : 
                            <Link activeClass="active" to={ navItem.node.id } spy={true} smooth={true} offset={-70} duration={500} className={classes.navLink}>
                                <ListItemText primary={'0' + (index + 1) + '. ' + navItem.node.title} />
                            </Link>
                        }
                        
                    </ListItem>
                ))}
            </List>
        </aside>
    )

    const container = window !== undefined ? () => window().document.body : undefined

    return (
        <div className={classes.root}>
            <Hidden mdUp implementation="css" className={ classes.hiddenMobile }>
                <div className={[classes.mobileNav, hideOnScroll ? classes.scrolling : '', isTop ? classes.top : ''].join(' ')}>
                    <Typography variant="h6" noWrap>Jóhannes Freyr</Typography>
                    <HamburgerMenu color="primary" onClick={handleDrawerToggle} aria-label="open-drawer" open={ mobileOpen } />
                </div>

                <nav className={classes.drawer} aria-label="mobile nav">
                    {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                    <Drawer
                        container={container}
                        variant="temporary"
                        anchor={'right'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {menuList}
                    </Drawer>
                </nav>
            </Hidden>
            <main className={classes.content}>
                {children}
            </main>
        </div>
    );
}

ResponsiveDrawer.propTypes = {
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window: PropTypes.func,
};

export default ResponsiveDrawer;
