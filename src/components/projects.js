import React, { useState } from 'react'
import { makeStyles, withStyles, useTheme } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import Box from '@material-ui/core/Box'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Container from '@material-ui/core/Container'
import { Grid } from '@material-ui/core'
import ToggleButton from '@material-ui/lab/ToggleButton'
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup'

import SectionHeader from '../components/sectionHeader'
import Project from '../components/Project/project'
import ProjectsHeader from '../components/Project/projectsHeader'

const useStyles = makeStyles((theme) => ({
    root: {
        position: "relative",
        minHeight: 600,
        [theme.breakpoints.up('md')]: {
            backgroundColor: theme.palette.primary.dark,
            marginLeft: 'calc((100% - 100vw) / 2)',
            marginRight: 'calc((100% - 100vw ) / 2)',
            padding: "300px 0px",
        },
    },
    pillHeader: {
        marginBottom: theme.spacing(4),
    },
    tabsContainer: {
        
        [theme.breakpoints.up('md')]: {
            flexGrow: 1,
            display: 'flex',
            backgroundColor: theme.palette.primary.dark,
        }
    },
    tabs: {
        [theme.breakpoints.up('md')]: {
            backgroundColor: theme.palette.primary.dark,
            borderRight: `1px solid ${theme.palette.secondary.main}`,
        }
    },

}))

const StyledContainer = withStyles(theme => ({
    root: {
        padding: 0,
        [theme.breakpoints.up('md')]: {
            backgroundColor: theme.palette.primary.dark,
            padding: `0 ${theme.spacing(7)}px`,
        }
    }
}))(Container)

const StyledTabs = withStyles(theme => ({
    root: {
        marginBottom: theme.spacing(4),
    },
    scroller: {
        [theme.breakpoints.down('md')]: {
            border: '1px solid rgba(255,255,255,0.25)',
            borderRadius: '3px'
        }
    },
    centered: {
        justifyContent: 'space-around'
    },
    indicator: {
        [theme.breakpoints.up('md')]: {
            right: 0,
        }
    }
}))(Tabs)

const StyledTab = withStyles(theme => ({
    root: {
        
        fontFamily: 'Share Tech Mono',
        textTransform: 'none',
        
        [theme.breakpoints.up('md')]: {
            marginLeft: -theme.spacing(3),
            textAlign: 'left',
        }
    },
    wrapper: {
        [theme.breakpoints.up('md')]: {
            alignItems: 'flex-start',
            paddingLeft: theme.spacing(2),
        }
    },
    selected: {
        color: theme.palette.secondary.main
    },
    
}))(Tab)

const StyledGrid = withStyles(theme => ({
    root: {
        marginTop: theme.spacing(10),
        marginBottom: theme.spacing(6),
        justifyContent: 'space-between',
        '& > div': {
            flexBasis: 'auto'
        }
    }
}))(Grid)

const StyledToggleButtonGroup = withStyles(theme => ({
    root: {
        color: theme.palette.secondary.main
    }
}))(ToggleButtonGroup)

const StyledToggleButton = withStyles(theme => ({
    root: {
        color: 'rgba(255,255,255,0.5)',
        borderColor: '#fff'
    },
    selected: {
        color: `${theme.palette.secondary.main} !important`
    }
}))(ToggleButton)

const a11yProps = (index) => {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

const TabPanel = (props) => {
    const { children, value, index, ...other } = props
    const theme = useTheme()
    const matchesBpUpMd = useMediaQuery(theme.breakpoints.up('md'))
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other} >
            {value === index && (
                <Box p={matchesBpUpMd ? 4 : 0 } pr={0}>
                    {children}
                </Box>
            )}
        </div>
    )
}

const Projects = () => {

    const [value, setValue] = useState(0)
    const [projectType, setProjectType] = useState('web')

    const classes = useStyles()
    const theme = useTheme()
    const matchesBpUpMd = useMediaQuery(theme.breakpoints.up('md'))
    const matchesBpDownMd = useMediaQuery(theme.breakpoints.down('md'))
    return (
        <section className={classes.root} id="projects">

            <StyledContainer>

                <div className={classes.pillHeader}>
                    <SectionHeader title="Recent Projects" variant="h3" index={2} useIndexPrefix />

                    <StyledGrid container spacing={matchesBpUpMd ? 4 : 0}>
                        <Grid item md={6}>
                            <ProjectsHeader type={projectType} />
                        </Grid>
                        <Grid item md={6}>
                            <StyledToggleButtonGroup aria-label="Select type of project" size="small" orientation={ matchesBpUpMd ? 'horizontal' : 'vertical' }>
                                <StyledToggleButton value="left" onClick={() => setProjectType('web')} selected={projectType === 'web'}>Web development</StyledToggleButton>
                                <StyledToggleButton value="right" onClick={() => setProjectType('branding')} selected={projectType === 'branding'}>Branding</StyledToggleButton>
                            </StyledToggleButtonGroup>
                        </Grid>

                    </StyledGrid>
                </div>


                <div className={classes.tabsContainer}>
                    {
                        projectType === 'web' ? <>
                            <StyledTabs
                                orientation={ matchesBpUpMd ? "vertical" : "horizontal" }
                                value={value}
                                onChange={(event, newValue) => setValue(newValue)}
                                aria-label="Recent Works"
                                className={classes.tabs}
                                centered={ matchesBpDownMd } >
                                <StyledTab label="meniga.is" {...a11yProps(0)} />
                                <StyledTab label="iuslaw.is" {...a11yProps(1)} />
                                <StyledTab label="payday.is" {...a11yProps(2)} />
                            </StyledTabs>
                            <TabPanel value={value} index={0}>
                                <Project 
                                    type="web"
                                    title="meniga.is" 
                                    description="A project that I worked on at Meniga ehf. The task was to implement a new look for the meniga.is* website and make it mobile friendly." 
                                    techStackItems={['HTML', 'Less', 'Bootstrap', 'React']}
                                    images={ ['meniga.png', 'gatsby-astronaut.png'] } />
                            </TabPanel>
                            <TabPanel value={value} index={1}>
                                <Project 
                                    type="web"
                                    title="iuslaw.is" 
                                    description="Design and implementation of new website for IUS law firm. The website is implemented with Angular and uses the Wordpress API." 
                                    techStackItems={['Photoshop', 'Angular', 'SSR', 'Bootstrap', 'Wordpress']} 
                                    images={['ius_logmenn.png', 'gatsby-astronaut.png']}/>
                            </TabPanel>
                            <TabPanel value={value} index={2}>
                                <Project
                                    type="web"
                                    title="payday.is" 
                                    description="Design and implementation of new website for IUS law firm. The website is implemented with Angular and uses the Wordpress API." 
                                    techStackItems={['Figma', 'Phototshop', 'JQuery', 'Less', 'C#', 'ASP.NET', 'Bootstrap']} 
                                    images={ ['payday_cover.jpg', 'payday_calculator.jpg', 'payday_invoice.png'] }/>
                            </TabPanel>


                        </> : <>
                                <div>Coming soon.. stay tuned</div>
                            </>
                    }


                </div>
            </StyledContainer>
        </section>
    )
}

export default Projects