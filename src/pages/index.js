import React from 'react'
import { createMuiTheme, responsiveFontSizes, ThemeProvider } from '@material-ui/core/styles'
import CssBaseLine from '@material-ui/core/CssBaseline'

import Layout from '../components/layout'
import SEO from '../components/seo'
import Head from '../components/head'
import Home from '../components/home'
import About from '../components/about'
import Contact from '../components/contact'
import Projects from '../components/projects'
import Experience from '../components/experience'

const customTheme = createMuiTheme({
    palette: {
        primary: {
            main: "#2A4558",
            dark: "#04101f"
        },
        secondary: {
            main: "#E9DE7C"
        },
    },
    body: {
        backgroundColor: "#2A4558",
        fontSize: 14,
    },
    typography: {
        fontFamily: ['"Saira"', '-apple-system', '"Helvetica Neue"', 'Arial', 'sans-serif'].join(','),
        fontSize: 14,
        h2: {
            fontWeight: 300,
            fontSize: '3.65rem'
        },
        h3: {
            fontSize: '2rem'
        },
        subtitle1: {
            fontSize: '0.875rem',
        }
    },
    text: {
        primary: '#fff',
        secondary: '#fff',
        disabled: '#fff',
        hint: '#fff',
        icon: '#fff',
    },
    props: {
        MuiTypography: {
            variantMapping: {
                subtitle1: 'h4',
                subtitle2: 'h5',
            }
        }
    },
    overrides: {
        MuiCssBaseline: {
            '@global': {
                body: {
                    color: "#fff",
                    backgroundColor: "#2A4558",
                    //fontSize: "1.0rem",
                },
            },
        },
    },
    MuiIconButton: {
        root: {
            color: '#fff',
        }
    },
    MuiSvgIcon: {
        root: {
            fill: '#fff',
        }
    },


})

let theme = customTheme
theme = responsiveFontSizes(theme)

const IndexPage = () => (

    <ThemeProvider theme={theme}>
        <CssBaseLine />
        <Layout>
            <SEO title="Home" />
            <Head />

            <Home />
            <About />
            <Experience />
            <Projects />
            <Contact />

        </Layout>
    </ThemeProvider>

)

export default IndexPage
